"__author__ = 'Leo Chan and Martin Fiser'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment 
"""

import pip
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'dateparser'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'boto'])

import csv
import time
from keboola import docker
import boto
from boto.s3.key import Key
from boto.exception import S3ResponseError
import datetime
import dateparser
import pandas as pd
import os
import io
import sys
import requests
import logging
import json
import gzip



# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
KEY_ID = cfg.get_parameters()["keyId"]
SKEY_ID = cfg.get_parameters()["#secret_key"]
BUCKET = cfg.get_parameters()["bucket_source"]
ABBREV = cfg.get_parameters()["area"] #folder_string
FILENAME = cfg.get_parameters()["file_name"] #file_name
include_file_name = cfg.get_parameters()["include_file_name"]
include_row_number = cfg.get_parameters()["include_row_number"]         # NEW
include_date = cfg.get_parameters()["include_date"]                     # NEW
FILENAME_COLUMN_NAME = cfg.get_parameters()["filename_column_name"]     # NEW: the column name for the FILENAME
EXTRACTION_COLUMN_NAME = cfg.get_parameters()["extraction_column_name"] # NEW: the column name for DATE
ROW_COLUMN_NAME = cfg.get_parameters()["row_column_name"]               # NEW: the column name for the index numbers
START_DATE = cfg.get_parameters()["start_date"]
END_DATE = cfg.get_parameters()["end_date"]
COLUMN_HEADER = cfg.get_parameters()["column_header"]

### Exit if there aren't any column headers
if len(COLUMN_HEADER) == 0:
    logging.error("Please enter column headers.")
    logging.error("Exit")
    sys.exit(0)

if include_file_name == "YES":
    INCLUDE_FILE_NAME = True
else:
    INCLUDE_FILE_NAME = False

if include_row_number == "YES":
    INCLUDE_ROW_NUMBER = True
else:
    INCLUDE_ROW_NUMBER = False

if include_date == "YES":
    INCLUDE_DATE = True
else:
    INCLUDE_DATE = False

# Get proper list of tables
cfg = docker.Config('/data/')
out_tables = cfg.get_expected_output_tables()
#logging.info("OUT tables mapped: "+str(out_tables))

DEFAULT_TABLE_INPUT = "/data/in/tables/"
DEFAULT_TABLE_OUTPUT = "/data/out/tables/"


class sfile:
    """
    represents files on S3
    """

    def __init__(self, name, size, modified, key):
        self.name = name
        self.size = size
        self.modified = modified
        self.key = key


def get_tables(out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["source"]
    logging.info("Output table: " + str(out_name))
    logging.info("Output table destination: " + str(out_Destination))

    return out_name


def dates_request(start_date, end_date):
    """
    Assemble list of dates based on the settings.
    """

    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            logging.error("ERROR: start_date cannot exceed end_date! Exit.")
            sys.exit(0)
        temp_date = start_date_form
        day_n = 0
        
        if day_diff == 0:
            ### normal with leading zeros
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
        
        while day_n < day_diff:
            ### normal with leading zeros 
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                ### normal with leading zeros
                dates.append(temp_date.strftime("%Y-%m-%d"))
                ### no leading zeros
                #dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        logging.error("ERROR: Please enter valid date parameters! Exit.")
        sys.exit(1)

    return dates


def list_buckets():
    """
    List buckets and their properties
    """

    try:
        buckets = conn.get_all_buckets()
    except (OSError,S3ResponseError) as e:
        logging.error("Could not list buckets. Please check credentials. Exit!")
        sys.exit(0)

    buckets_names = []

    for i in buckets:
        name = i.name
        buckets_names.append(name)

    return buckets_names


def list_items(bucket_name, folder=""):
    """
    list items in the bucket.
    returns a list of objects
    
    supported info:
        .name
        .key
        .size
        .last_modified
    
    if nonexistent is None:
        print "No such bucket!"
    """

    bucket = conn.get_bucket(bucket_name, validate=False)
    items = []

    for i in bucket.list(prefix=folder):
        name = i.name
        location = i.key
        size = i.size
        modified = i.last_modified

        info = sfile(name, size, modified, location)
        items.append(info)

    return items


def output_file(data_output, file_output, column_headers):
    """ 
    Output the dataframe input to destination file
    Append to the file if file does not exist
    * row by row
    """

    if not os.path.isfile(file_output):
        with open(file_output, "a") as b:
            data_output.to_csv(b, index=False, columns=column_headers)
        b.close()
    else:
        with open(file_output, "a") as b:
            data_output.to_csv(b, index=False, header=False, columns=column_headers)
        b.close()

    return


def main():
    """
    Main execution script.
    """

    bucket_name = BUCKET
    folder_string = ABBREV
    OUTPUT_FILE = get_tables(out_tables) 

    """ Generate a list of dates to download based on dates request """
    NOW = dateparser.parse("today").strftime("%Y-%m-%d %H:%M:%S %z")
    dates = dates_request(START_DATE, END_DATE)

    """ Find out which files need to be outputed """
    logging.info("Getting file list.")
    itemslist = list_items(bucket_name, folder_string)
    full_list = []

    bad_dates = []
    date_format = "YYYY-MM-DD"

    for j in dates:
        string = FILENAME.replace(date_format, str(j))

        """ NEW """
        count = 0
        string_list = []
        for i in string:
            if i == "*":
                count+=1
        if count == 0:
            string_list.append(string)
        else:
            string_list = string.split("*")

        bool_exist = False
        for i in itemslist:
            temp_bool = True
            for k in string_list:
                if k in (i.name):
                    pass
                else:
                    temp_bool = temp_bool and False
            if temp_bool:
                full_list.append(i.name)
            bool_exist = bool_exist or temp_bool
        
        if bool_exist==False:
            logging.info("{0} does not contain any data.".format(string))
            bad_dates.append(j)
    
    """ Output by files """
    output_df = pd.DataFrame()
    good_output = []
    bad_output = []

    logging.info("File count: "+str(len(full_list)))
    logging.info("Starting file downloads.")

    for i in full_list:
        file_source = i
        file_name = i.split(".csv")[0]
        file_input = DEFAULT_TABLE_INPUT+file_name+".gz"

        bucket = conn.get_bucket(bucket_name, validate=False)
        k = Key(bucket,file_source)
        
        try:
            k.get_contents_to_filename(file_input)

            """ Reading the source file as DF"""
            logging.info(file_source+" downloaded.")
            data = pd.read_table(file_input, compression="zip", dtype=str, sep=",", header=None)
            data.columns = COLUMN_HEADER
            column_header = []
            for i in COLUMN_HEADER:
                column_header.append(i)

            if INCLUDE_FILE_NAME:
                data[str(FILENAME_COLUMN_NAME)] = file_source
                column_header.append(str(FILENAME_COLUMN_NAME))
                
            if INCLUDE_ROW_NUMBER:
                data[str(ROW_COLUMN_NAME)] = data.index.map(int).astype(int)+1
                column_header.append(str(ROW_COLUMN_NAME))

            if INCLUDE_DATE:
                data[str(EXTRACTION_COLUMN_NAME)] = NOW
                column_header.append(str(EXTRACTION_COLUMN_NAME))

            """ Output """
            file_output = file_name+".csv"

            """ Append the modified data to the output table """
            output_file(data, OUTPUT_FILE, column_header)
            del data
            good_output.append(i)      
        except S3ResponseError:
            logging.error("ERROR: Could not retrieve {0}. Check storage class?".format(i))
        except Exception as b:
            logging.error("ERROR: {0} - {1}".format(i,b))
            bad_output.append(i)

    return



if __name__ == "__main__":
    try:
        conn = boto.connect_s3(KEY_ID,SKEY_ID)
        logging.info("Successfully connected."+str(conn))
    except Exception as a:
        logging.error("Could not connect. Exit!")
        sys.exit(1)

    main()
    
    logging.info("Done.")
